#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
//#include <gmp.h>
#include "mini-gmp.h"
#include "mini-mpq.h" 

mpq_t** read_matrix(char* fname,int* n );
void write_matrix(mpq_t** M,int n);
int find_pivot(mpq_t** M,int n, int cur);
void subtract_row(mpq_t lambda, mpq_t ** M, int n, int source, int target);
void normalize_row(mpq_t** M, int n, int cur);
void forward_elim_step(mpq_t** M,int n, int cur);
void backward_elim_step(mpq_t** M,int n, int cur);

void gauss(mpq_t ** M, int n);

