#include "gauss.h" 

//reads a list of doubles from file fname, uses the first number as an size n and the remaining numbers as the entries of an nx(n+1) matrix
mpq_t** read_matrix(char* fname,int* n ){
	FILE *file;
	file = fopen(fname,"r");
	char num[1000];
	fscanf(file,"%i",n);
	mpq_t** M = (mpq_t**) malloc(sizeof(mpq_t *)*(*n));
	for (int i = 0; i<*n; i++)
		M[i] = (mpq_t*)malloc(sizeof(mpq_t)*((*n)+1));
	for ( int i = 0; i < (*n); i++)
		for ( int j = 0; j < (*n)+1; j++){
			fscanf(file,"%s",num);
			mpq_init(M[i][j]);
			mpq_set_str(M[i][j],num,10);
			mpq_canonicalize(M[i][j]);
			//printf(" M[%i,%i] is %f\n",i,j,mpq_get_d(M[i][j]));
		}
	fclose(file);
	return M;
}

void write_matrix(mpq_t** M,int n){
	for ( int i = 0; i < n; i++){
		for ( int j = 0; j < n+1; j++){
			printf("%f ",mpq_get_d(M[i][j]));
		}
	printf("\n");
	}
}

//finds index of first non zero entry starting from position [cur,cur] moving down
int find_pivot(mpq_t** M,int n, int cur){
	for(int i = cur; i<n ;i++)
		if(mpq_sgn(M[i][cur])!=0)
			return i;
	return -1;
}

//subtracts lambda times row i from row j
void subtract_row(mpq_t lambda, mpq_t ** M, int n, int source, int target){
	//printf("lambda is %f.\n",mpq_get_d(lambda));
	mpq_t temp;
	mpq_init(temp);
	for(int k = 0; k<n+1;k++){
		mpq_mul(temp,M[source][k], lambda);
		mpq_sub(M[target][k],M[target][k],temp);
		//printf("k is %i.\n",k);
		//printf("M[target][k] is %f.\n",mpq_get_d(M[target][k]));
		//printf("temp is %f.\n",mpq_get_d(temp));
	}
	mpq_clear(temp);
}

//normalizes row cur, s.t. M[cur][cur] becomes 1.
void normalize_row(mpq_t** M, int n, int cur){
	mpq_t normalization_factor;
	mpq_init(normalization_factor);
	mpq_set(normalization_factor,M[cur][cur]);
	for(int i = 0;i<n+1;i++)
		mpq_div(M[cur][i],M[cur][i],normalization_factor);
	mpq_clear(normalization_factor);
}

//uses entry [cur,cur] to eliminate all entries below.
void forward_elim_step(mpq_t** M,int n, int cur){
	normalize_row(M,n,cur);
	// gmp variables are pass-by-reference so passing M[i][cur] directly to subtract_row wont fly
	mpq_t lambda;
	mpq_init(lambda);
	for(int i = cur+1; i<n ;i++){
		mpq_set(lambda,M[i][cur]);
		subtract_row(lambda, M, n, cur, i);
	}
	mpq_clear(lambda);
}

//uses entry [cur,cur] to eliminate all entries above.
void backward_elim_step(mpq_t** M,int n, int cur){
	mpq_t lambda;
	mpq_init(lambda);
	for(int i = cur-1; i>= 0;i--){
		mpq_set(lambda,M[i][cur]);
		subtract_row(lambda, M, n, cur, i);
	}
	mpq_clear(lambda);
}

void gauss(mpq_t ** M, int n){
	int piv;
	mpq_t * temp;

	//printf("starting forward elimination.\n");
	for(int cur = 0; cur<n ;cur++){
		piv = find_pivot(M,n,cur);
		//printf("%i th pivot is %i.\n",cur,piv);
		//swap pivot pivot row to current row
		temp = M[cur];
		M[cur] = M[piv];
		M[piv] = temp;
		forward_elim_step(M,n,cur);
	}
	//printf("finished forward elimination.\n");
	//printf("starting backward elimination.\n");
	for(int cur = n-1; cur>0;cur--){
		backward_elim_step(M,n,cur);
	}
	//backward elimination
}

