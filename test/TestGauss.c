#include <stdio.h>
#include <stdlib.h>
#include "gauss.h"
#include "unity.h"

//#include <gmp.h>
#include "mini-gmp.h"
#include "mini-mpq.h"

mpq_t** M;
mpq_t** P;
mpq_t** Q;
int n;

void setUp(void)
{
	//Matrix M
	n = 4;
	M = (mpq_t**) malloc(sizeof(mpq_t *)*(n));
	for (int i = 0; i < n; i++){
		M[i] = (mpq_t*)malloc(sizeof(mpq_t)*(n+1));
		for (int j = 0; j<n; j++)
			mpq_init(M[i][j]);
	}
	int i = 0;
	//row 0
	mpq_set_str(M[i/5][i%5],"1",10);
	i++;
	mpq_set_str(M[i/5][i%5],"4/2",10);
	i++;
	mpq_set_str(M[i/5][i%5],"-2",10);
	i++;
	mpq_set_str(M[i/5][i%5],"4",10);
	i++;
	mpq_set_str(M[i/5][i%5],"15",10);
	i++;
	//row 1
	mpq_set_str(M[i/5][i%5],"3",10);
	i++;
	mpq_set_str(M[i/5][i%5],"1",10);
	i++;
	mpq_set_str(M[i/5][i%5],"2",10);
	i++;
	mpq_set_str(M[i/5][i%5],"4",10);
	i++;
	mpq_set_str(M[i/5][i%5],"27",10);
	i++;
	//row 2
	mpq_set_str(M[i/5][i%5],"-1",10);
	i++;
	mpq_set_str(M[i/5][i%5],"-3",10);
	i++;
	mpq_set_str(M[i/5][i%5],"-1",10);
	i++;
	mpq_set_str(M[i/5][i%5],"-5",10);
	i++;
	mpq_set_str(M[i/5][i%5],"-30",10);
	i++;	
	//row 3
	mpq_set_str(M[i/5][i%5],"2",10);
	i++;
	mpq_set_str(M[i/5][i%5],"1",10);
	i++;
	mpq_set_str(M[i/5][i%5],"2",10);
	i++;
	mpq_set_str(M[i/5][i%5],"3",10);
	i++;
	mpq_set_str(M[i/5][i%5],"22",10);
	i++;

	for (int i = 0; i<n; i++)
		for (int j = 0; j<n+1; j++)
			mpq_canonicalize(M[i][j]);

	//Matrix P
	P = (mpq_t**) malloc(sizeof(mpq_t *)*(n));
	for (int i = 0; i < n; i++){
		P[i] = (mpq_t*)malloc(sizeof(mpq_t)*(n+1));
		for (int j = 0; j<n; j++)
			mpq_init(P[i][j]);
	}
	i = 0;
	//row 0
	mpq_set_str(P[i/5][i%5],"1/2",10);
	i++;
	mpq_set_str(P[i/5][i%5],"0",10);
	i++;
	mpq_set_str(P[i/5][i%5],"-2",10);
	i++;
	mpq_set_str(P[i/5][i%5],"4",10);
	i++;
	mpq_set_str(P[i/5][i%5],"15",10);
	i++;
	//row 1
	mpq_set_str(P[i/5][i%5],"3",10);
	i++;
	mpq_set_str(P[i/5][i%5],"0",10);
	i++;
	mpq_set_str(P[i/5][i%5],"2",10);
	i++;
	mpq_set_str(P[i/5][i%5],"4",10);
	i++;
	mpq_set_str(P[i/5][i%5],"27",10);
	i++;
	//row 2
	mpq_set_str(P[i/5][i%5],"-1",10);
	i++;
	mpq_set_str(P[i/5][i%5],"3",10);
	i++;
	mpq_set_str(P[i/5][i%5],"-1",10);
	i++;
	mpq_set_str(P[i/5][i%5],"-5",10);
	i++;
	mpq_set_str(P[i/5][i%5],"-30",10);
	i++;	
	//row 3
	mpq_set_str(P[i/5][i%5],"2",10);
	i++;
	mpq_set_str(P[i/5][i%5],"0",10);
	i++;
	mpq_set_str(P[i/5][i%5],"2",10);
	i++;
	mpq_set_str(P[i/5][i%5],"3",10);
	i++;
	mpq_set_str(P[i/5][i%5],"22",10);
	i++;

	for (int i = 0; i<n; i++)
		for (int j = 0; j<n+1; j++)
			mpq_canonicalize(P[i][j]);

		//Matrix Q
	Q = (mpq_t**) malloc(sizeof(mpq_t *)*(n));
	for (int i = 0; i < n; i++){
		Q[i] = (mpq_t*)malloc(sizeof(mpq_t)*(n+1));
		for (int j = 0; j<n; j++)
			mpq_init(Q[i][j]);
	}
	i = 0;
	//row 0
	mpq_set_str(Q[i/5][i%5],"2",10);
	i++;
	mpq_set_str(Q[i/5][i%5],"1",10);
	i++;
	mpq_set_str(Q[i/5][i%5],"3",10);
	i++;
	mpq_set_str(Q[i/5][i%5],"0",10);
	i++;
	mpq_set_str(Q[i/5][i%5],"8",10);
	i++;
	//row 1
	mpq_set_str(Q[i/5][i%5],"0",10);
	i++;
	mpq_set_str(Q[i/5][i%5],"2",10);
	i++;
	mpq_set_str(Q[i/5][i%5],"6",10);
	i++;
	mpq_set_str(Q[i/5][i%5],"0",10);
	i++;
	mpq_set_str(Q[i/5][i%5],"4",10);
	i++;
	//row 2
	mpq_set_str(Q[i/5][i%5],"0",10);
	i++;
	mpq_set_str(Q[i/5][i%5],"0",10);
	i++;
	mpq_set_str(Q[i/5][i%5],"1",10);
	i++;
	mpq_set_str(Q[i/5][i%5],"0",10);
	i++;
	mpq_set_str(Q[i/5][i%5],"1",10);
	i++;	
	//row 3
	mpq_set_str(Q[i/5][i%5],"0",10);
	i++;
	mpq_set_str(Q[i/5][i%5],"0",10);
	i++;
	mpq_set_str(Q[i/5][i%5],"0",10);
	i++;
	mpq_set_str(Q[i/5][i%5],"0",10);
	i++;
	mpq_set_str(Q[i/5][i%5],"0",10);
	i++;

	for (int i = 0; i<n; i++)
		for (int j = 0; j<n+1; j++)
			mpq_canonicalize(Q[i][j]);
}

void tearDown(void)
{
	for (int i = 0; i<n; i++){
		free(M[i]);
		free(P[i]);
		free(Q[i]);
	}
	free(M);
	free(P);	
	free(Q);
}


void test_read_matrix(){
	int n;
	mpq_t** N = read_matrix("test/matrix.in",&n);
	TEST_ASSERT_EQUAL_INT(n,4);
	for (int i = 0; i<n; i++)
		for (int j = 0; j<n; j++)
			TEST_ASSERT_EQUAL_DOUBLE(mpq_get_d(M[i][j]),mpq_get_d(N[i][j]));
	for (int i = 0; i<n; i++)
		free(N[i]);
	free(N);
}

void test_find_pivot(){
	TEST_ASSERT_EQUAL_INT(2, find_pivot(P, n, 1));
}


void test_normalize_row_negative_numbers(){
	normalize_row(M, n, 2);
	double expected[3] = {1,5,30};
	double actual [3];
	for (int j = 0; j<3; j++)
		actual[j] = mpq_get_d(M[2][2+j]);
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(expected, actual, 3);
}

void test_normalize_row_fractions(){
	normalize_row(P, n, 0);
	double expected[5] = {1,0,-4,8,30};
	double actual [5];
	for (int j = 0; j<5; j++)
		actual[j] = mpq_get_d(P[0][j]);
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(expected, actual, 5);
}

void test_subtract_row(){
	mpq_t lambda;
	mpq_init(lambda);
	mpq_set_str(lambda,"3",10);
	subtract_row(lambda, M, n, 0, 1);
	double expected[5] = {0,-5,8,-8,-18};
	double actual[5];
	for (int j = 0; j<5; j++)
		actual[j] = mpq_get_d(M[1][j]);
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(expected, actual, 5);
}
void test_forward_elim_step(){
	forward_elim_step(M, n, 0);
	double expected[5] = {0,-5,8,-8,-18};
	double actual[5];
	for (int j = 0; j<5; j++)
		actual[j] = mpq_get_d(M[1][j]);
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(expected, actual, 5);

	double expected2 [5] = {0,-1,-3,-1,-15};
	for (int j = 0; j<5; j++)
		actual[j] = mpq_get_d(M[2][j]);
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(expected2, actual, 5);
}

void test_backward_elim_step(){
	backward_elim_step(Q, n, 2);
	double expected[5] = {0,2,0,0,-2};
	double actual[5];
	for (int j = 0; j<5; j++)
		actual[j] = mpq_get_d(Q[1][j]);
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(expected, actual, 5);

	double expected2 [5] = {2,1,0,0,5};
	for (int j = 0; j<5; j++)
		actual[j] = mpq_get_d(Q[0][j]);
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(expected2, actual, 5);
}

void test_gauss(){
	gauss(M,n);
	double expected[20] ={	1,0,0,0,1,
							0,1,0,0,2,
							0,0,1,0,3,
							0,0,0,1,4};
	double actual[20]; 
	for (int j = 0; j<20; j++)
		actual[j] = mpq_get_d(M[j/5][j%5]);
	TEST_ASSERT_EQUAL_DOUBLE_ARRAY(expected, actual, 20);
}

